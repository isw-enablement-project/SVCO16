openapi: 3.0.0
info:
  title: Party Lifecycle Management API
  description: >+
    No toolkit to be used in IBM Business Automation Workflow has been created
    for v1. You can enable that in the Solution Designer on API Namespace level.

  contact: {}
  version: 1.0.0
servers:
  - url: https://education-dev.apps.openshift-01.knowis.cloud/partylife/api/v1
security:
  - oauth2schema: []
paths:
  /login:
    post:
      tags: []
      operationId: PLMLogin
      parameters:
        - name: Accept-Language
          in: header
          description: >-
            List of acceptable human languages for response. According to `RFC
            7231`
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-ParentSpanId
          in: header
          description: Zipkin Parent Span Id (e.g. `6adc7f8b31f35e91`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-SpanId
          in: header
          description: Zipkin Span Id (e.g. `0a7b5cd9c2568f7d`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-TraceId
          in: header
          description: Zipkin Trace Id (e.g. `0a7b5cd9c2568f7d6adc7f8b31f35e91`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-Sampled
          in: header
          description: Zipkin Sampled (e.g. `0`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-DEBUG-SESSION
          in: header
          description: X-DEBUG-SESSION
          required: false
          style: simple
          explode: false
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginBody'
        required: false
      responses:
        '200':
          description: Login response on success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
        '401':
          description: Unauthorized error when login credential is incorrect
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
        '404':
          description: Not found when user does not exist
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
  /login/{loginId}:
    get:
      tags: []
      operationId: GetUserLogin
      parameters:
        - name: loginId
          in: path
          required: true
          style: simple
          explode: false
          schema:
            type: string
        - name: Accept-Language
          in: header
          description: >-
            List of acceptable human languages for response. According to `RFC
            7231`
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-ParentSpanId
          in: header
          description: Zipkin Parent Span Id (e.g. `6adc7f8b31f35e91`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-SpanId
          in: header
          description: Zipkin Span Id (e.g. `0a7b5cd9c2568f7d`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-TraceId
          in: header
          description: Zipkin Trace Id (e.g. `0a7b5cd9c2568f7d6adc7f8b31f35e91`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-B3-Sampled
          in: header
          description: Zipkin Sampled (e.g. `0`)
          required: false
          style: simple
          explode: false
          schema:
            type: string
        - name: X-DEBUG-SESSION
          in: header
          description: X-DEBUG-SESSION
          required: false
          style: simple
          explode: false
          schema:
            type: string
      responses:
        '200':
          description: Login response on success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
        '401':
          description: Unauthorized error when login credential is incorrect
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
        '404':
          description: Not found when user does not exist
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
components:
  schemas:
    LoginBody:
      required:
        - id
        - password
      type: object
      properties:
        id:
          type: string
        password:
          type: string
    LoginResponse:
      type: object
      properties:
        result:
          type: string
          enum:
            - SUCCESS
            - FAILED
  responses: {}
  parameters:
    LoginId:
      name: loginId
      in: path
      required: true
      style: simple
      explode: false
      schema:
        type: string
    Accept-Language:
      name: Accept-Language
      in: header
      description: List of acceptable human languages for response. According to `RFC 7231`
      required: false
      style: simple
      explode: false
      schema:
        type: string
    X-B3-ParentSpanId:
      name: X-B3-ParentSpanId
      in: header
      description: Zipkin Parent Span Id (e.g. `6adc7f8b31f35e91`)
      required: false
      style: simple
      explode: false
      schema:
        type: string
    X-B3-SpanId:
      name: X-B3-SpanId
      in: header
      description: Zipkin Span Id (e.g. `0a7b5cd9c2568f7d`)
      required: false
      style: simple
      explode: false
      schema:
        type: string
    X-B3-TraceId:
      name: X-B3-TraceId
      in: header
      description: Zipkin Trace Id (e.g. `0a7b5cd9c2568f7d6adc7f8b31f35e91`)
      required: false
      style: simple
      explode: false
      schema:
        type: string
    X-B3-Sampled:
      name: X-B3-Sampled
      in: header
      description: Zipkin Sampled (e.g. `0`)
      required: false
      style: simple
      explode: false
      schema:
        type: string
    X-DEBUG-SESSION:
      name: X-DEBUG-SESSION
      in: header
      description: X-DEBUG-SESSION
      required: false
      style: simple
      explode: false
      schema:
        type: string
  requestBodies: {}
  securitySchemes:
    oauth2schema:
      type: oauth2
      flows:
        implicit:
          authorizationUrl: >-
            https://keycloak-edu-keycloak.apps.openshift-01.knowis.cloud/auth/realms/education/protocol/openid-connect/auth
          scopes: {}
      x-tokenName: id_token
x-knowis-solutionAcronym: PARTYLIFE
x-knowis-apiName: Party Lifecycle Management API
x-knowis-namespacePrefix: v1
x-knowis-ibmBpmIntegration: false
x-knowis-ibmApiConnectIntegration: false
x-knowis-language: java
x-knowis-supportedspec: openApi3.0
